#include "catch.hpp"

#include "../riziko/card.h"
#include "../riziko/cardtask.h"
#include "../riziko/destroyplayercard.h"
#include "../riziko/game.h"
#include "../riziko/initializer.h"
#include "../riziko/player.h"
#include "../riziko/region.h"
#include "../riziko/regionoccupycard.h"
#include "../riziko/territory.h"
#include "../riziko/territoryoccupycard.h"

#include <climits>

TEST_CASE("Remove player", "[player]") {

    Game *g = new Game();
    Player *p1 = new Player("1", Color::RED);
    Player *p2 = new Player("2", Color::BLUE);
    Player *p3 = new Player("3", Color::PURPLE);
    Player *p4 = new Player("4", Color::YELLOW);
    Player *p5 = new Player("5", Color::GREEN);
    QVector<Player*> players{p1,p2,p3,p4,p5};
    g->setPlayers(players);

    SECTION("Provera broja igraca nakon izbacivanja") {
        g->removePlayer(p2);
        CHECK(g->players().size() == 4);
    }

    SECTION("Uklanjanje prvog igraca") {
        g->removePlayer(p1);
        QVector<Player*> izlaz{p2,p3,p4,p5};

        for(int i = 0; i < izlaz.size(); i++) {
            REQUIRE(izlaz[i] == g->players()[i]);
        }
    }

    SECTION("Uklanjanje poslednjeg igraca") {
        g->removePlayer(p5);
        QVector<Player*> izlaz{p1,p2,p3,p4};

        for(int i = 0; i < izlaz.size(); i++) {
            REQUIRE(izlaz[i] == g->players()[i]);
        }
    }

    SECTION("Uklanjanje random igraca") {
        g->removePlayer(p3);
        QVector<Player*> izlaz{p1,p2,p4,p5};

        for(int i = 0; i < izlaz.size(); i++) {
            REQUIRE(izlaz[i] == g->players()[i]);
        }
    }

    SECTION("Uklanjanje igraca koji ne postoji") {
        Player* randomPlayer = new Player();
        g->removePlayer(randomPlayer);
        QVector<Player*> izlaz{p1,p2,p3,p4,p5};

        for(int i = 0; i < izlaz.size(); i++) {
            REQUIRE(izlaz[i] == g->players()[i]);
        }
    }
}


TEST_CASE("Game attack", "[attack]")
{
    Territory *t1 = new Territory();
    Territory *t2 = new Territory();
    QVector<int> dices;
    Game *g = new Game();

    SECTION("Provera kockica na osnovu broja tenkica na teritorijama napadaca I slucaj")
    {
        t1->setNumOfTanks(2);
        t2->setNumOfTanks(5);
        dices = g->attack(t1,t2);
        REQUIRE(dices[1] == 0);
        REQUIRE(dices[2] == 0);
    }

    SECTION("Provera kockica na osnovu broja tenkica na teritorijama napadaca II slucaj")
    {
        t1->setNumOfTanks(3);
        t2->setNumOfTanks(5);
        dices = g->attack(t1,t2);
        REQUIRE(dices[2] == 0);
    }


    SECTION("Provera kockica na osnovu broja tenkica na teritorijama branioca I slucaj")
    {
        t1->setNumOfTanks(5);
        t2->setNumOfTanks(1);
        dices = g->attack(t1,t2);
        REQUIRE(dices[4] == 0);
        REQUIRE(dices[5] == 0);
    }

    SECTION("Provera kockica na osnovu broja tenkica na teritorijama branioca II slucaj")
    {
        t1->setNumOfTanks(5);
        t2->setNumOfTanks(2);
        dices = g->attack(t1,t2);
        REQUIRE(dices[5] == 0);
    }


    SECTION("Provera kockica na osnovu broja tenkica na teritorijama napadaca I slucaj")
    {
        t1->setNumOfTanks(2);
        t2->setNumOfTanks(5);
        dices = g->attack(t1,t2);
        REQUIRE(dices[1] == 0);
        REQUIRE(dices[2] == 0);
    }

    SECTION("Provera sortiranja kockica")
    {
        t1->setNumOfTanks(5);
        t2->setNumOfTanks(5);
        dices = g->attack(t1,t2);
        CHECK(dices[0] >= dices[1]);
        CHECK(dices[1] >= dices[2]);
        CHECK(dices[3] >= dices[4]);
        CHECK(dices[4] >= dices[5]);
    }

    SECTION("Provera napada") {
        t1->setNumOfTanks(5);
        t2->setNumOfTanks(5);
        int num1 = t1->numOfTanks();
        int num2 = t2->numOfTanks();
        dices = g->attack(t1, t2);

        if(dices[0] > dices[3]){
            CHECK_FALSE(t2->numOfTanks() == num2);
        } else if (dices[0] <= dices[3]) {
            CHECK_FALSE(t1->numOfTanks() == num1);
        }

        if(dices[1] > dices[4]){
            CHECK_FALSE(t2->numOfTanks() == num2);
        } else if (dices[1] <= dices[4]) {
            CHECK_FALSE(t1->numOfTanks() == num1);
        }

        if(dices[2] > dices[5]){
            CHECK_FALSE(t2->numOfTanks() == num2);
        } else if (dices[2] <= dices[5]){
            CHECK_FALSE(t1->numOfTanks() == num1);
        }
    }
}


TEST_CASE("Add tanks to territory", "[territory]")
{
    Territory *t1 = new Territory();
    int br = t1->numOfTanks();
    Game *g = new Game();

    SECTION("Povecanje broja tenkova na teritoriji - 0")
    {
     g->addTanksToTerritory(0, t1);
     CHECK(t1->numOfTanks() == br);
    }

    SECTION("Povecanje broja tenkova na teritoriji - int_max")
    {
     g->addTanksToTerritory(INT_MAX, t1);
     CHECK(t1->numOfTanks() == br + INT_MAX);
    }
    SECTION("Povecanje broja tenkova na teritoriji - int_min")
    {
     g->addTanksToTerritory(INT_MIN, t1);
     CHECK(t1->numOfTanks() == br + INT_MIN);
    }

    SECTION("Povecanje broja tenkova na teritoriji - -1")
    {
     g->addTanksToTerritory(-1, t1);
     CHECK(t1->numOfTanks() == br -1);
    }
}

