# Podešavanje jedne partije igre

**Kratak opis**: Vrši se odabir  opcije "JOIN GAME" iz glavnog menija. Nakon toga svaki od igrača bira svoje ime i boju i priključuje se igri.

**Akteri**:  Igrači

**Preduslovi**:  Aplikacija prikazuje glavni meni.

**Postuslovi**:  Igra je pokrenuta.

**Osnovni tok**:
 1.  Igrač unosi ime i vrši se provera da li je neko već uzeo to ime.
 2.  Igrač bira boju.
 3.  Priključuje se igri klikom na dugme "ADD PLAYER".
 4.  Kada se priključe svi igrači, klikom na dugme "START" započinje sama igra.

**Alternativni tokovi**: /

**Podtokovi**: /

**Specijalni zahtevi**:  /

**Dodatne informacije**: /

