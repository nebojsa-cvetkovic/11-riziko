#ifndef INITIALIZER_H
#define INITIALIZER_H

#include <stdio.h>

#include <QFile>
#include <QHash>
#include <QString>
#include <QTextStream>
#include <QVector>
#include <algorithm>
#include <fstream>
#include <iostream>

#include "card.h"
#include "cardtask.h"
#include "region.h"

class Player;

class Initializer {
 private:
  static QSet<Territory*> makeAdjTerritories(QStringList&, QHash<QString, Territory*>&);

 public:
  Initializer();
  inline static QHash<QString, Territory*> allTerritories;
  inline static QHash<int, Card*> allCards;

  void initializeCardTasks(QVector<Player*>&);
  QSet<Territory*> initializerTerritory(QVector<Player*>&);
  QVector<Region*> initializeRegions();
  QSet<Card*> initializeCards();
  bool checkTask(Player*);
};

#endif  // INITIALIZER_H
