/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../riziko/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[57];
    char stringdata0[1050];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 19), // "currentIndexChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 21), // "on_pbJoinGame_clicked"
QT_MOC_LITERAL(4, 54, 18), // "on_pbStart_clicked"
QT_MOC_LITERAL(5, 73, 17), // "on_pbBack_clicked"
QT_MOC_LITERAL(6, 91, 21), // "on_pbDrawCard_clicked"
QT_MOC_LITERAL(7, 113, 19), // "on_pbAttack_clicked"
QT_MOC_LITERAL(8, 133, 18), // "on_pbCards_clicked"
QT_MOC_LITERAL(9, 152, 22), // "on_pbExitCards_clicked"
QT_MOC_LITERAL(10, 175, 21), // "on_pbNextCard_clicked"
QT_MOC_LITERAL(11, 197, 22), // "on_pbAddPlayer_clicked"
QT_MOC_LITERAL(12, 220, 17), // "on_pbExit_clicked"
QT_MOC_LITERAL(13, 238, 21), // "on_pbPlayMove_clicked"
QT_MOC_LITERAL(14, 260, 25), // "on_pbExcangeCards_clicked"
QT_MOC_LITERAL(15, 286, 10), // "updateCbTo"
QT_MOC_LITERAL(16, 297, 1), // "n"
QT_MOC_LITERAL(17, 299, 11), // "updateCbNum"
QT_MOC_LITERAL(18, 311, 10), // "startTimer"
QT_MOC_LITERAL(19, 322, 12), // "timerTimeout"
QT_MOC_LITERAL(20, 335, 23), // "on_pbNextPlayer_clicked"
QT_MOC_LITERAL(21, 359, 18), // "on_Albania_clicked"
QT_MOC_LITERAL(22, 378, 18), // "on_Estonia_clicked"
QT_MOC_LITERAL(23, 397, 18), // "on_Austria_clicked"
QT_MOC_LITERAL(24, 416, 18), // "on_Belarus_clicked"
QT_MOC_LITERAL(25, 435, 18), // "on_Belgium_clicked"
QT_MOC_LITERAL(26, 454, 17), // "on_Bosnia_clicked"
QT_MOC_LITERAL(27, 472, 19), // "on_Bulgaria_clicked"
QT_MOC_LITERAL(28, 492, 18), // "on_Croatia_clicked"
QT_MOC_LITERAL(29, 511, 16), // "on_Czech_clicked"
QT_MOC_LITERAL(30, 528, 18), // "on_Denmark_clicked"
QT_MOC_LITERAL(31, 547, 18), // "on_Finland_clicked"
QT_MOC_LITERAL(32, 566, 17), // "on_France_clicked"
QT_MOC_LITERAL(33, 584, 18), // "on_Germany_clicked"
QT_MOC_LITERAL(34, 603, 17), // "on_Greece_clicked"
QT_MOC_LITERAL(35, 621, 18), // "on_Hungary_clicked"
QT_MOC_LITERAL(36, 640, 18), // "on_Iceland_clicked"
QT_MOC_LITERAL(37, 659, 18), // "on_Ireland_clicked"
QT_MOC_LITERAL(38, 678, 16), // "on_Italy_clicked"
QT_MOC_LITERAL(39, 695, 17), // "on_Latvia_clicked"
QT_MOC_LITERAL(40, 713, 20), // "on_Lithuania_clicked"
QT_MOC_LITERAL(41, 734, 20), // "on_Macedonia_clicked"
QT_MOC_LITERAL(42, 755, 18), // "on_Moldova_clicked"
QT_MOC_LITERAL(43, 774, 22), // "on_Netherlands_clicked"
QT_MOC_LITERAL(44, 797, 17), // "on_Norway_clicked"
QT_MOC_LITERAL(45, 815, 17), // "on_Poland_clicked"
QT_MOC_LITERAL(46, 833, 19), // "on_Portugal_clicked"
QT_MOC_LITERAL(47, 853, 18), // "on_Romania_clicked"
QT_MOC_LITERAL(48, 872, 17), // "on_Serbia_clicked"
QT_MOC_LITERAL(49, 890, 19), // "on_Slovakia_clicked"
QT_MOC_LITERAL(50, 910, 19), // "on_Slovenia_clicked"
QT_MOC_LITERAL(51, 930, 16), // "on_Spain_clicked"
QT_MOC_LITERAL(52, 947, 17), // "on_Sweden_clicked"
QT_MOC_LITERAL(53, 965, 22), // "on_Switzerland_clicked"
QT_MOC_LITERAL(54, 988, 18), // "on_Ukraine_clicked"
QT_MOC_LITERAL(55, 1007, 24), // "on_UnitedKingdom_clicked"
QT_MOC_LITERAL(56, 1032, 17) // "on_pbMove_clicked"

    },
    "MainWindow\0currentIndexChanged\0\0"
    "on_pbJoinGame_clicked\0on_pbStart_clicked\0"
    "on_pbBack_clicked\0on_pbDrawCard_clicked\0"
    "on_pbAttack_clicked\0on_pbCards_clicked\0"
    "on_pbExitCards_clicked\0on_pbNextCard_clicked\0"
    "on_pbAddPlayer_clicked\0on_pbExit_clicked\0"
    "on_pbPlayMove_clicked\0on_pbExcangeCards_clicked\0"
    "updateCbTo\0n\0updateCbNum\0startTimer\0"
    "timerTimeout\0on_pbNextPlayer_clicked\0"
    "on_Albania_clicked\0on_Estonia_clicked\0"
    "on_Austria_clicked\0on_Belarus_clicked\0"
    "on_Belgium_clicked\0on_Bosnia_clicked\0"
    "on_Bulgaria_clicked\0on_Croatia_clicked\0"
    "on_Czech_clicked\0on_Denmark_clicked\0"
    "on_Finland_clicked\0on_France_clicked\0"
    "on_Germany_clicked\0on_Greece_clicked\0"
    "on_Hungary_clicked\0on_Iceland_clicked\0"
    "on_Ireland_clicked\0on_Italy_clicked\0"
    "on_Latvia_clicked\0on_Lithuania_clicked\0"
    "on_Macedonia_clicked\0on_Moldova_clicked\0"
    "on_Netherlands_clicked\0on_Norway_clicked\0"
    "on_Poland_clicked\0on_Portugal_clicked\0"
    "on_Romania_clicked\0on_Serbia_clicked\0"
    "on_Slovakia_clicked\0on_Slovenia_clicked\0"
    "on_Spain_clicked\0on_Sweden_clicked\0"
    "on_Switzerland_clicked\0on_Ukraine_clicked\0"
    "on_UnitedKingdom_clicked\0on_pbMove_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      54,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  284,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  287,    2, 0x08 /* Private */,
       4,    0,  288,    2, 0x08 /* Private */,
       5,    0,  289,    2, 0x08 /* Private */,
       6,    0,  290,    2, 0x08 /* Private */,
       7,    0,  291,    2, 0x08 /* Private */,
       8,    0,  292,    2, 0x08 /* Private */,
       9,    0,  293,    2, 0x08 /* Private */,
      10,    0,  294,    2, 0x08 /* Private */,
      11,    0,  295,    2, 0x08 /* Private */,
      12,    0,  296,    2, 0x08 /* Private */,
      13,    0,  297,    2, 0x08 /* Private */,
      14,    0,  298,    2, 0x08 /* Private */,
      15,    1,  299,    2, 0x08 /* Private */,
      17,    1,  302,    2, 0x08 /* Private */,
      18,    0,  305,    2, 0x08 /* Private */,
      19,    0,  306,    2, 0x08 /* Private */,
      20,    0,  307,    2, 0x08 /* Private */,
      21,    0,  308,    2, 0x08 /* Private */,
      22,    0,  309,    2, 0x08 /* Private */,
      23,    0,  310,    2, 0x08 /* Private */,
      24,    0,  311,    2, 0x08 /* Private */,
      25,    0,  312,    2, 0x08 /* Private */,
      26,    0,  313,    2, 0x08 /* Private */,
      27,    0,  314,    2, 0x08 /* Private */,
      28,    0,  315,    2, 0x08 /* Private */,
      29,    0,  316,    2, 0x08 /* Private */,
      30,    0,  317,    2, 0x08 /* Private */,
      31,    0,  318,    2, 0x08 /* Private */,
      32,    0,  319,    2, 0x08 /* Private */,
      33,    0,  320,    2, 0x08 /* Private */,
      34,    0,  321,    2, 0x08 /* Private */,
      35,    0,  322,    2, 0x08 /* Private */,
      36,    0,  323,    2, 0x08 /* Private */,
      37,    0,  324,    2, 0x08 /* Private */,
      38,    0,  325,    2, 0x08 /* Private */,
      39,    0,  326,    2, 0x08 /* Private */,
      40,    0,  327,    2, 0x08 /* Private */,
      41,    0,  328,    2, 0x08 /* Private */,
      42,    0,  329,    2, 0x08 /* Private */,
      43,    0,  330,    2, 0x08 /* Private */,
      44,    0,  331,    2, 0x08 /* Private */,
      45,    0,  332,    2, 0x08 /* Private */,
      46,    0,  333,    2, 0x08 /* Private */,
      47,    0,  334,    2, 0x08 /* Private */,
      48,    0,  335,    2, 0x08 /* Private */,
      49,    0,  336,    2, 0x08 /* Private */,
      50,    0,  337,    2, 0x08 /* Private */,
      51,    0,  338,    2, 0x08 /* Private */,
      52,    0,  339,    2, 0x08 /* Private */,
      53,    0,  340,    2, 0x08 /* Private */,
      54,    0,  341,    2, 0x08 /* Private */,
      55,    0,  342,    2, 0x08 /* Private */,
      56,    0,  343,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   16,
    QMetaType::Void, QMetaType::QString,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentIndexChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->on_pbJoinGame_clicked(); break;
        case 2: _t->on_pbStart_clicked(); break;
        case 3: _t->on_pbBack_clicked(); break;
        case 4: _t->on_pbDrawCard_clicked(); break;
        case 5: _t->on_pbAttack_clicked(); break;
        case 6: _t->on_pbCards_clicked(); break;
        case 7: _t->on_pbExitCards_clicked(); break;
        case 8: _t->on_pbNextCard_clicked(); break;
        case 9: _t->on_pbAddPlayer_clicked(); break;
        case 10: _t->on_pbExit_clicked(); break;
        case 11: _t->on_pbPlayMove_clicked(); break;
        case 12: _t->on_pbExcangeCards_clicked(); break;
        case 13: _t->updateCbTo((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 14: _t->updateCbNum((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 15: _t->startTimer(); break;
        case 16: _t->timerTimeout(); break;
        case 17: _t->on_pbNextPlayer_clicked(); break;
        case 18: _t->on_Albania_clicked(); break;
        case 19: _t->on_Estonia_clicked(); break;
        case 20: _t->on_Austria_clicked(); break;
        case 21: _t->on_Belarus_clicked(); break;
        case 22: _t->on_Belgium_clicked(); break;
        case 23: _t->on_Bosnia_clicked(); break;
        case 24: _t->on_Bulgaria_clicked(); break;
        case 25: _t->on_Croatia_clicked(); break;
        case 26: _t->on_Czech_clicked(); break;
        case 27: _t->on_Denmark_clicked(); break;
        case 28: _t->on_Finland_clicked(); break;
        case 29: _t->on_France_clicked(); break;
        case 30: _t->on_Germany_clicked(); break;
        case 31: _t->on_Greece_clicked(); break;
        case 32: _t->on_Hungary_clicked(); break;
        case 33: _t->on_Iceland_clicked(); break;
        case 34: _t->on_Ireland_clicked(); break;
        case 35: _t->on_Italy_clicked(); break;
        case 36: _t->on_Latvia_clicked(); break;
        case 37: _t->on_Lithuania_clicked(); break;
        case 38: _t->on_Macedonia_clicked(); break;
        case 39: _t->on_Moldova_clicked(); break;
        case 40: _t->on_Netherlands_clicked(); break;
        case 41: _t->on_Norway_clicked(); break;
        case 42: _t->on_Poland_clicked(); break;
        case 43: _t->on_Portugal_clicked(); break;
        case 44: _t->on_Romania_clicked(); break;
        case 45: _t->on_Serbia_clicked(); break;
        case 46: _t->on_Slovakia_clicked(); break;
        case 47: _t->on_Slovenia_clicked(); break;
        case 48: _t->on_Spain_clicked(); break;
        case 49: _t->on_Sweden_clicked(); break;
        case 50: _t->on_Switzerland_clicked(); break;
        case 51: _t->on_Ukraine_clicked(); break;
        case 52: _t->on_UnitedKingdom_clicked(); break;
        case 53: _t->on_pbMove_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MainWindow::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::currentIndexChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 54)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 54;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 54)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 54;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::currentIndexChanged(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
