#ifndef PLAYER_H
#define PLAYER_H
#include <QString>
#include <QVector>
#include <QSet>

// #include "card.h"
// #include "territory.h"
#include "cardtask.h"

enum class Color
{
    RED,
    GREEN,
    BLUE,
    YELLOW,
    PURPLE,
};

class Card;
class Territory;

class Player
{
public:
    Player();
    Player(const QString &name, Color color, bool ownerInd);
    const QString &name() const;

    void setName(const QString &newName);
    QString getName();
    Color color() const;
    void setColor(Color newColor);
    std::int32_t numOfTerritories() const;
    bool ownderInd() const;
    void setOwnerInd(bool newOwnerInd);
    Player* playerToDestroy() const;
    void setPlayerToDestroy(Player* newPlayerToDestroy);

    void addCard(Card*);
    void addTerritory(Territory*);
    void removeTerritory(Territory*);
    void removeCard(Card*);
    int getTanks(Card*, Card*, Card*);
    void moveTanks(Territory*, Territory*, int);

    void addRandomTer(QString); //ogi
    QSet<QString> getRandomTer();
    void addCardTask(CardTask*); // ovo treba da bude CardTask tip
    CardTask* cardTask() const; // ovo treba da bude CardTask tip

    const QSet<Territory *> &territories() const;
    void setTerritories(const QSet<Territory *> &newTerritories);

    const QVector<Card *> &cards() const;
    void setCards(const QVector<Card *> &newCards);

    void setNumOfTanks(int num);
    int getNumOfTanks();
    QString toString();

private:
    QString m_name;
    Color m_color;
    bool m_ownerInd;
    Player* m_playerToDestroy = nullptr;
    QVector<Card*> m_cards;
    QSet<Territory*> m_territories;
    CardTask* m_cardTask;
    int m_numOfTanksToAdd;
};

#endif // PLAYER_H
