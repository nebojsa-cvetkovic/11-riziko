#ifndef DESTROYPLAYERCARD_H
#define DESTROYPLAYERCARD_H

#include "cardtask.h"
#include "player.h"

class DestroyPlayerCard : public CardTask
{
public:
    DestroyPlayerCard();
    DestroyPlayerCard(const QString&, Player *playerToDestroy);
    Player *playerToDestroy() const;
    bool checkTask(Player *) const override;
    ~DestroyPlayerCard();

private:
    Player * m_playerToDestroy;
};

#endif // DESTROYPLAYERCARD_H
