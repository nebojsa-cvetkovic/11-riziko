#ifndef REGIONOCCUPYCARD_H
#define REGIONOCCUPYCARD_H

#include <QSet>
#include "region.h"
#include "cardtask.h"

class RegionOccupyCard : public CardTask
{
public:
    RegionOccupyCard();
    RegionOccupyCard(const QString &, QVector<Region*> &, std::int32_t numOfRegions);
    QVector<Region*> regions() const;
    std::int32_t numOfRegions() const;
    bool checkTask(Player *) const override;
    ~RegionOccupyCard() {};

private:
    QVector<Region*> m_regions;
    std::int32_t m_numOfRegions;

};

#endif // REGIONOCCUPYCARD_H
