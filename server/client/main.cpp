#include "tcpclient/tcpclient.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    TcpClient w;

    QObject::connect(&w, &TcpClient::closeGame, &a, &QApplication::quit);
    w.show();
    return a.exec();
}
